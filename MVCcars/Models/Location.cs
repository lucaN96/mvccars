﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCcars.Models
{
    public class Location
    {
        public string Id { get; set; }
       
        public double Latitude { get; set; }
        
        public double Longitude { get; set; }
    }
}
