﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCcars.Models
{
    public class Company
    {
        public string Id { get; set; }
        public string ExternalId { get; set; }
        [Display(Name= "Company Name")]
        public string Name { get; set; }
        public Contact Contact { get; set; }
        public Address Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Company()
        {

        }
    }
}
