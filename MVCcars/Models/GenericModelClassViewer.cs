﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCcars.Models
{
    public class GenericModelClassViewer<T>
    {
        public List<T> list;

        public string Searched { get; set; }
    }
}
