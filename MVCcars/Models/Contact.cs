﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCcars.Models
{
    public class Contact
    {
        public string Id { get; set; }
        [Display(Name="Contact Name")]
        public string Name { get; set; }
        public string Title { get; set; }
    }
}
