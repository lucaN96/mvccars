﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVCcars.Models
{
    public class Employee
    {
        public string Id { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string Title { get; set; }
        public Address Address { get; set; }
        [Display(Name = "Hired At")]
        [DataType(DataType.Date)]
        public DateTime HiredAt { get; set; }
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }
        public string Extension { get; set; }
        [NotMapped]
        public string ReportsTo { get; set; }
        [NotMapped]
        public List<string> Notes { get; set; }
        [NotMapped]
        public List<string> Territories { get; set; }

        public Employee()
        {

        }
    }
}
