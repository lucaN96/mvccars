﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVCcars.Models;
using Raven.Client.Documents;

namespace MVCcars.Controllers
{
    public class SuppliersController : Controller
    {
        private readonly MVCcarsContext _context;

        public SuppliersController(MVCcarsContext context)
        {
            _context = context;
        }

        // GET: Suppliers
        public IActionResult Index(string searched)
        {
            using (var session = StoreHolder.Store.OpenSession())
            {
                var supplViewer = new GenericModelClassViewer<Supplier> { };

                if (!String.IsNullOrEmpty(searched))
                {   //wait for non stale - attende l'aggiornamento del db, aspetta che l'indice sia aggiornato
                    supplViewer.list = session.Query<Supplier>().Customize(q => q.WaitForNonStaleResults(TimeSpan.FromSeconds(2))).Search(x => x.Name, "*" + searched + "*").Where(x => x.Name != null).OrderBy(z => z.Name).ToList();

                }
                else
                {

                    supplViewer.list = session.Query<Supplier>().Customize(aq => aq.WaitForNonStaleResults(TimeSpan.FromSeconds(2))).Where(x => x.Name != null).OrderBy(z => z.Name).ToList();

                }


                return View(supplViewer);
            }

        }

        // GET: Suppliers/Details/5
        public IActionResult Details(string id)
        {
            return MyViewCaller("Details", id);
            
        }

        // GET: Suppliers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Suppliers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,Phone,Fax,HomePage,Contact,Address")] Supplier supplier)
        {
            using (var session = StoreHolder.Store.OpenSession())
            {
                if (ModelState.IsValid)
                {
                    session.Store(supplier);
                    session.SaveChanges();

                }
                else
                {
                    return View(supplier);
                }


            }
            return RedirectToAction(nameof(Index));

        }

        // GET: Suppliers/Edit/5
        public IActionResult Edit(string id)
        {
            return MyViewCaller("Edit", id);
            
        }

        // POST: Suppliers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("Id,Name,Phone,Fax,HomePage,Contact,Address")] Supplier supplier)
        {
            if (id != supplier.Id)
            {
                return NotFound();
            }
            AdjustIdName(ref id);
            if (ModelState.IsValid)
            {
                using (var session = StoreHolder.Store.OpenSession())
                {

                    supplier.Id = id;
                    session.Store(supplier);
                    session.SaveChanges();
                }
                return RedirectToAction(nameof(Index));
            }
            return View(supplier);

        }

        // GET: Suppliers/Delete/5
        public IActionResult Delete(string id)
        {
            return View("Delete", id);


        }

        // POST: Suppliers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            AdjustIdName(ref id);
            using (var session = StoreHolder.Store.OpenSession())
            {
                session.Delete(id);
                session.SaveChanges();
                return RedirectToAction(nameof(Index));
            }

        }
        private void AdjustIdName(ref string id)
        {
            id = "suppliers/" + id;
        }
        private IActionResult MyViewCaller(string view, string id)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            id = "suppliers/" + id;
            using (var session = StoreHolder.Store.OpenSession())
            {
                var supplier = session.Query<Supplier>().Where(x => x.Id.Equals(id)).FirstOrDefault();
                if (supplier == null)
                {
                    return RedirectToAction(nameof(Index));
                }
                return View(view, supplier);
            }

        }
    }
}
