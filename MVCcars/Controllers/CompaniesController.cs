﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVCcars.Models;
using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Raven.Client.Documents.Session;

namespace MVCcars.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly MVCcarsContext _context;

        public CompaniesController(MVCcarsContext context)
        {
            _context = context;
        }

        // GET: Companies
        public IActionResult Index(string searched)
        {
            var x = new GenericModelClassViewer<Company>();

            using (var session = StoreHolder.Store.OpenSession())
            {
                var query = session.Query<Company>().Where(n => n.Name != null);

                if (!String.IsNullOrEmpty(searched))
                {
                    //wait for non stale ---attende l'aggiornamento del db, aspetta che l'indice sia aggiornato
                    //se non lo aggiungo, dopo che ho inserito un nuovo utente e sono ridirezionato ad Index, non vedo il nuovo inserimento
                    x.list = session.Query<Company>().Customize(q => q.WaitForNonStaleResults(TimeSpan.FromSeconds(2))).Search(r => r.Name, "*" + searched + "*").OrderBy(z => z.ExternalId).ToList();
                    
                }
                else
                {
                    x.list = query.Customize(q => q.WaitForNonStaleResults(TimeSpan.FromSeconds(2))).OrderBy(r => r.ExternalId).ToList();
                }

            }
            return View(x);
        }

        // GET: Companies/Details/5
        public  IActionResult Details(string id)
        {
            return MyViewCaller("Details", id);    
        }

        // GET: Companies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,ExternalId,Name,Phone,Fax,Contact,Address")] Company company)
        {
            if (ModelState.IsValid)
            {
                using (var session = StoreHolder.Store.OpenSession())
                {
                    session.Store(company);
                    var i = company.Id;
                    session.SaveChanges();
                }

                return RedirectToAction(nameof(Index));
            }
            return View(company);
        }

        // GET: Companies/Edit/5
        public IActionResult Edit(string id)
        {
            return MyViewCaller("Edit", id);
      
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("Id,ExternalId,Name,Phone,Fax,Contact,Address")] Company company)
        {
            if (id != company.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                id = "companies/" + id;
                using (var session = StoreHolder.Store.OpenSession())
                {
                    
                    company.Id = id;
                    session.Store(company);
                    session.SaveChanges();
                }
                
                return RedirectToAction(nameof(Index));
            }
            return View(company);
        }

        // GET: Companies/Delete/5
        public IActionResult Delete(string id)
        {
            return MyViewCaller("Delete", id);
            
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            using (var session = StoreHolder.Store.OpenSession())
            {
                id = "companies/" + id;
                
                session.Delete(id);
                session.SaveChanges();
            }
           
            return RedirectToAction(nameof(Index));
        }
        private void AdjustId(ref string id)
        {
            id = "companies/" + id;
        }

        private IActionResult MyViewCaller(string view, string id)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Index));
            }

            id = "companies/" + id;
            using (var session = StoreHolder.Store.OpenSession())
            {
                var company = session.Query<Company>().Where(x => x.Id.Equals(id)).FirstOrDefault();
                if (company == null)
                {
                    return RedirectToAction(nameof(Index));
                }
                return View(view, company);
            }

        }

    }
}
