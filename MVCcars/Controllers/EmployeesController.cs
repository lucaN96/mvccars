﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVCcars.Models;
using Raven.Client.Documents;

namespace MVCcars.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly MVCcarsContext _context;

        public EmployeesController(MVCcarsContext context)
        {
            _context = context;
        }

        // GET: Employees
        public IActionResult Index(string searched)
        {
            using (var session = StoreHolder.Store.OpenSession())
            {
                var v = new GenericModelClassViewer<Employee>();
                if (!String.IsNullOrEmpty(searched))
                {
                    var q = session.Query<Employee,SearchIndex>()
                        .Customize(aq => aq.WaitForNonStaleResults(TimeSpan.FromSeconds(2))) //attende l'aggiornamento del db, aspetta che l'indice sia aggiornato
                        .Where(x => x.FirstName != null)
                        .Search(s => s.FirstName, "*" + searched + "*") 
                        .OrderBy(x => x.FirstName); //avendo indicizzato solo FirstName nella classe indice, non posso ordinare su FirstName, perchè non è ritornato dalla query
                    v.list = q.ToList();
                }
                else
                {
                    //in questa query non sto usando l'indice (secondo generics nel metodo query), quindi posso ordinare secondo LastName
                    v.list = session.Query<Employee>()
                        .Customize(aq => aq.WaitForNonStaleResults(TimeSpan.FromSeconds(2)))
                        .Where(x => x.FirstName != null).OrderBy(x => x.FirstName).ToList();
                }


                return View(v);
            }

        }

        // GET: Employees/Details/5
        public IActionResult Details(string id)
        {
            
            return MyViewCaller("Details", id);

        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,LastName,FirstName,Title,HiredAt,Birthday,HomePhone,Extension,ReportsTo")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                using (var session = StoreHolder.Store.OpenSession())
                {
                    session.Store(employee);
                    session.SaveChanges();

                }
                return RedirectToAction(nameof(Index));

            }
            return View(employee);
        }

        // GET: Employees/Edit/5
        public IActionResult Edit(string id)
        {
            
            return MyViewCaller("Edit", id);

        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("Id,LastName,FirstName,Title,HiredAt,Birthday,HomePhone,Extension,ReportsTo")] Employee employee)
        {
            if (id != employee.Id)
            {
                return RedirectToAction(nameof(Index));
            }
            AdjustId(ref id);
            if (ModelState.IsValid)
            {
                using (var session = StoreHolder.Store.OpenSession())
                {
                    employee.Id = id;
                    session.Store(employee);
                    session.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }

            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public IActionResult Delete(string id)
        {
            
            return MyViewCaller("Delete", id);

        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            AdjustId(ref id);
            using (var session = StoreHolder.Store.OpenSession())
            {
                session.Delete(id);
                session.SaveChanges();
                return RedirectToAction(nameof(Index));
            }

        }
        private void AdjustId(ref string id)
        {
            id = "employees/" + id;
        }
       
        private IActionResult MyViewCaller(string view,string id)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            id = "employees/" + id;
            using (var session = StoreHolder.Store.OpenSession())
            {
                var employee = session.Query<Employee>().Where(x => x.Id.Equals(id)).FirstOrDefault();
                if (employee == null)
                {
                    return RedirectToAction(nameof(Index));
                }
                return View(view, employee);
            }
            
        }
       
    }
}
