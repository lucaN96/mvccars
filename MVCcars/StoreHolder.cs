﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Raven.Client;
using Raven.Client.Documents;
using Raven.Client.Documents.Indexes;

namespace MVCcars
{
    public static class StoreHolder
    {
        private static readonly Lazy<IDocumentStore> store =
            new Lazy<IDocumentStore>(() =>
            {
                var s = new DocumentStore
                {
                    Urls = new[] { "http://localhost:8080" },
                    Database = "Northwind"
                };
                s.Initialize();
                var asm = Assembly.GetExecutingAssembly();
                IndexCreation.CreateIndexes(asm, s); //ask the client to find all indexes classes and send them to the server
                //I don't have to do them manually with "new Indexclass().execute(store)"

                return s;
            });
        public static IDocumentStore Store => store.Value;
    }
}
