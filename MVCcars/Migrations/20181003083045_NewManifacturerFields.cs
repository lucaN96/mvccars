﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVCcars.Migrations
{
    public partial class NewManifacturerFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AnnoFondazione",
                table: "Manifacturer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Nazionalita",
                table: "Manifacturer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnnoFondazione",
                table: "Manifacturer");

            migrationBuilder.DropColumn(
                name: "Nazionalita",
                table: "Manifacturer");
        }
    }
}
