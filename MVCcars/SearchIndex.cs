﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Raven.Client.Documents.Indexes;


namespace MVCcars
{
    public class SearchIndex :  AbstractIndexCreationTask<Models.Employee>
    {
        //indicizzo la classe Employee, creando l'indice sul campo FirstName che è l'unione di First e Last.
        public SearchIndex()
        {
            Map = employees => (
                from e in employees
                select new 
                {
                    //posso usare solo FirstName nelle query in employee, dove uso l'indice SearchIndex
                    FirstName = e.FirstName + " " + e.LastName 
                }


            );

            //AddMap<Models.Company>(c =>
            //        from companies in c
            //        select new Result
            //        {
            //           Name = companies.Name
            //        });
            //AddMap<Models.Supplier>(s => 
            //                from suppliers in s
            //                select new Result
            //                {
            //                   Name =  suppliers.Name
            //                });

            //AddMap<Models.Employee>(e =>
            //                from employees in e
            //                select new Result
            //                {
            //                    Name = employees.FirstName + " " + employees.LastName
            //                });
            

        }
    }
}
