﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVCcars.Models;

namespace MVCcars.Models
{
    public class MVCcarsContext : DbContext
    {
        public MVCcarsContext (DbContextOptions<MVCcarsContext> options)
            : base(options)
        {
        }

       
        public DbSet<MVCcars.Models.Company> Company { get; set; }

        public DbSet<MVCcars.Models.Supplier> Supplier { get; set; }

        public DbSet<MVCcars.Models.Employee> Employee { get; set; }

    }
}
